## 25/11/2022 
I moved everything to https://github.com/vanphong93/movie-react-javascript, something fix and delete.

# Movie project
Welcome to the Movie website. This is a project programmed by two web developers. First of all, I would like to introduce web developer Ho Van Phong, who made the Layout part of the Website and included some functions such as login, registration, or movie ticket booking. And the other is Huynh Duc Phat who built the AdminPage function for the website.
Thank for watching !
### TaskTable
https://docs.google.com/spreadsheets/d/1dh5HAVg43sEXvlipIRlmFeuqyNWF7rrrCjb9wZQEn7A/edit#gid=0
### WireFrames
https://drive.google.com/drive/folders/1UrpPa--LO8qIP8SY8YnGJXckikhGwpEd?usp=sharing
### Deployment
 https://my-movie-web.vercel.app/
## Requests

- Create an account or you can use :
userAccount (1vanphong1010/123456)

adminAccount(admin005/admin003)
- You cannot access the site AdminPage without an admin account.


## Install
In the project directory, you can run:

```sh
npm i
npm start
```
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
### Third Party libraries used except for React and Tailwind

- [react-rounter-dom](https://reactrouter.com/en/main)
- [formik](https://formik.org/)
- [axios](https://github.com/axios/axios)
- [antd](https://ant.design/)
- [react-slick](https://react-slick.neostack.com/)
- [react-redux](https://react-redux.js.org/)
- [redux-thunk](https://github.com/reduxjs/redux-thunk)
- [react-spinners](https://github.com/davidhu2000/react-spinners)
- [lottie-react](https://github.com/Gamote/lottie-react)

